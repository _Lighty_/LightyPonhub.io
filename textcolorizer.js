// Text Colorizer by David at stuffbydavid.com, 2011
// Really old code ahead!

function cutHex(h) {
  return (h.charAt(0) == '#') ? h.substring(1, 7) : h
}
function hexToR(h) {
  return parseInt((cutHex(h)).substring(0, 2), 16)
}
function hexToG(h) {
  return parseInt((cutHex(h)).substring(2, 4), 16)
}
function hexToB(h) {
  return parseInt((cutHex(h)).substring(4, 6), 16)
}
function rgbToHex(R, G, B) {
  return toHex(R) + toHex(G) + toHex(B)
}
function toHex(n) {
  n = parseInt(n, 10);
  if (isNaN(n)) return '00';
  n = Math.max(0, Math.min(n, 255));
  return '0123456789ABCDEF'.charAt((n - n % 16) / 16) +
      '0123456789ABCDEF'.charAt(n % 16);
}
input_effect = ''
input_color1 = ''
input_color2 = ''
input_color3 = ''
input_color4 = ''
input_color5 = ''
input_color6 = ''
input_color7 = ''
input_color8 = ''
input_text = ''
input_font = ''
input_size = ''
input_bold = 0
input_italic = 0
input_colorword = 0
random_length = 0;
update = 0
var random_char = new Array();
function randomize_colors() {
  var length = document.getElementById('input_text').value.length;
  var a;
  for (a = 0; a < length; a += 1) {
    random_char[a] = rgbToHex(
        Math.floor(Math.random() * 256), Math.floor(Math.random() * 256),
        Math.floor(Math.random() * 256))
  }
  random_length = length;
  update = 1
}

function textcolorizer_handle() {
  if (input_text != document.getElementById('input_text').value) {
    update = 1;
  }
  input_text = document.getElementById('input_text').value;

  if (input_bold != document.getElementById('input_bold').checked) {
    update = 1;
  }
  input_bold = document.getElementById('input_bold').checked;

  if (input_italic != document.getElementById('input_italic').checked) {
    update = 1;
  }
  input_italic = document.getElementById('input_italic').checked;

  if (update == 1) {
    update = 0;
    str_html = '';
    str_bbcode = '';
    str_sourcemod = '';
    var str_bbcodeend = '';
    incompatible = '';
    if (input_bold || input_italic || input_font)
      incompatible = 'Incompatible settings:'
    str_style = '';
    if (input_bold == 1) {
      str_style += 'font-weight:bold;';
      str_bbcode += '[b]';
      str_bbcodeend = '[/b]' + str_bbcodeend;
      incompatible += ' Bold'
    }
    if (input_italic == 1) {
      str_style += 'font-style:italic;';
      str_bbcode += '[i]';
      str_bbcodeend = '[/i]' + str_bbcodeend;
      incompatible += ' Italic'
    }
    if (str_style != '') str_html += '<span style=\'' + str_style + '\'>';
    var a, r, g, b, rinc, ginc, binc, ccol;

      var i, s, p;
      for (a = 0; a < input_text.length; a++) {
        i = a / input_text.length;
        s = 1 / 6
        p = (i % s) / s
        if (i >= s * 0) ccol = rgbToHex(255, 255 * p, 0);
        if (i >= s * 1) ccol = rgbToHex(255 * (1 - p), 255, 0);
        if (i >= s * 2) ccol = rgbToHex(0, 255, 255 * p);
        if (i >= s * 3) ccol = rgbToHex(0, 255 * (1 - p), 255);
        if (i >= s * 4) ccol = rgbToHex(255 * p, 0, 255);
        if (i >= s * 5) ccol = rgbToHex(255, 0, 255 * (1 - p));
        if (input_text.charAt(a) == ' ') {
          str_html += ' ';
          str_bbcode += ' ';
          str_sourcemod += ' ';
        } else {
          str_html += '<span style=\'color:#' + ccol + ';\'>' +
              input_text.charAt(a) + '</span>';
          str_bbcode +=
              '[color=#' + ccol + ']' + input_text.charAt(a) + '[/color]';
          str_sourcemod += '' + ccol + input_text.charAt(a);
        }
      }

    if (str_style != '') {
      str_html += '</span>'
    }
    if (incompatible != '') {
      document.getElementById('div_incompatible').innerHTML = incompatible;
    }
    else
      document.getElementById('div_incompatible').innerHTML = '';

    if (str_bbcode.length + str_bbcodeend.length > 400) {
      document.getElementById('div_errorBBCode').innerHTML = 'Text too long for shoutbox!';
    }
    else
      document.getElementById('div_errorBBCode').innerHTML = '';

    if (str_sourcemod.length > 192) {
      document.getElementById('div_errorSM').innerHTML = 'Too long for in-game text!';
    }
    else
      document.getElementById('div_errorSM').innerHTML = '';

    document.getElementById('div_preview').innerHTML =
        '<span style=\'font-size:12px\'>' + str_html + '</span>';
    document.getElementById('output_bbcode').value = str_bbcode + str_bbcodeend;
    document.getElementById('output_sourcemod').value = str_sourcemod;
  }
  setTimeout(textcolorizer_handle, 50)
}